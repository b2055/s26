const http = require("http");

//Create a variable "port" to store the port number
const port = 4000;

//Create a variable "server" that stores the output of the "create server" method

const server = http.createServer((request, response) => {

    if(request.url == '/greeting') {
        response.writeHead(200, {"Content-Type":"text/plain"})
        response.end("Hello again")
        
    } else if (request.url == '/homepage') {
        response.writeHead(200, {"Content-Type":"text/plain"})
        response.end("This is the homepage")
    } else {
        response.writeHead(404, {"Content-Type":"text/plain"})
        response.end("Page not found")
    }

})

//Use the "server" and "port" variabled created above

server.listen(port);

//When server is running, console will print the message:
console.log(`Server now accesible at localhost ${port}`)