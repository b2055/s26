const http = require("http");
const port = 3000;

const server = http.createServer((request, response) => {

    switch(request.url) {
        case "/login":
            response.writeHead(200, {"Content-Type":"text/plain"})
            response.end("You are in the login page")
            break;
        default: 
            response.writeHead(404, {"Content-Type":"text/plain"})
            response.end("Page not found")
    }
})

server.listen(port);

console.log("Server is successfully running")